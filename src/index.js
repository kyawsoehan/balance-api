const express = require("express")
const cors = require("cors")
const bodyParser = require("body-parser")

//load env variables
require("dotenv").config()

//application code import
const env = require("./environments")
const userController = require("./user/user.controller")


const app = express()


//apply system middlewares
app.use(bodyParser.json())
app.use(cors())


//set application controllers url
app.use("/users", userController)

//error handler at global level
app.use((err, req, res, next) => {
    console.error(`Error at global level`)
    console.error(err)

    const status = err.status || 500
    const message = err.message || "Internal Server Error"

    return res.status(status).json({
        message
    })
})


app.listen(env.SERVER_PORT, () => {
    console.log(`Server is running at ${env.SERVER_PORT}`)
})
