const SERVER_PORT = process.env.SERVER_PORT

const CRYPTO_PRICE_URL = process.env.CRYPTO_PRICE_URL

module.exports = {
    SERVER_PORT,
    CRYPTO_PRICE_URL
}