const env = require("../environments")
const apiUtil = require('../common/api.util')

/*
    return type will be
    {
        "err": if something goes wrong (price will be empty)
        "price": price for particular crypto and currency pair type
    }
*/
function getPrice(cryptoType){

    const uri = getUri(cryptoType)
    const url = `${env.CRYPTO_PRICE_URL}/${uri}`

    return apiUtil.getApi(url)
            .then(res => res.data)
            .then(json => {
                return {
                    err: undefined,
                    price: json.last
                }
            })
            .catch(err => {
                return {
                    err,
                    price: undefined
                }
            })
}

function getUri(cryptoType){
    return cryptoType.toLowerCase() + "usd"
}

function calculateAmount(cryptoPrice, amount){
    return cryptoPrice * amount
}

module.exports = {
    getPrice,
    calculateAmount,
    getUri
}