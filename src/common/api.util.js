const axios = require("axios")

function getApi(url){
    return axios.get(url)
}

module.exports = {
    getApi
}