const userBalances = {
    "user-1": {
      "BTC": "0.5",
      "ETH": "2",
    },
    "user-2": {
      "BTC": "0.1",
    },
    "user-3": {
      "ETH": "5"
    },
  }

//this function will try to mock what real db returns
function findUserCryptoById(userId){

    const userData = userBalances[userId]
    if(!userId){
        return Promise.resolve([])
    }

    const result = []
    for(const prop in userData){
        result.push({
            cryptoType: prop,
            amount: Number(userData[prop])
        })
    }
    return Promise.resolve(result)
}

module.exports = {
    findUserCryptoById
}