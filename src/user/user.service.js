const cryptoUtil = require("../common/crypto.util")
const userRepo = require("./user.repo")

async function getUserBalance(userId){

    const userData = await userRepo.findUserCryptoById(userId)
    if(!userData || userData.length === 0){
        return 0
    }

    const promises = userData.map(({cryptoType, amount}) => {
        return cryptoUtil.getPrice(cryptoType)
            .then(({err, price}) => {
                if(err){
                    throw err
                }
                return cryptoUtil.calculateAmount(price, amount)
            })
    })
    return Promise.all(promises)
        .then(values => values.reduce((prevValue, currValue) => prevValue + currValue))
}

module.exports = {
    getUserBalance
}