const express = require("express")

const router = express.Router()

const userService = require("./user.service")

router.get("/:userId", async (req, res, next) => {

    const { userId } = req.params
    if(!userId){
        return res.status(400).json({
            message: "User ID is required"
        })
    }

    try{
        const balance = await userService.getUserBalance(userId)
        return res.json({
            balance
        })
    }catch(err){
        next(err)
    }
})

//error handling at controller level
router.use((err, req, res, next) => {
    
    console.error("Error for user request")
    console.error(req)
    console.error(err.message)

    return res.status(500).json({
        message: "Internal Server Error"
    })
})

module.exports = router