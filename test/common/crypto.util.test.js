const spies = require("chai-spies")
const chai = require("chai")
const cryptoUtil = require("../../src/common/crypto.util")
const apiUtil = require("../../src/common/api.util")
const env = require("../../src/environments")

//set up environment variables
require("dotenv").config

chai.use(spies)
const { expect, spy } = require("chai")

describe("Testing crypto.util.js", () => {

    describe("Test getUri", () => {

        it("should return btcusd", () => {
            const cryptoType = "btc"
            const desiredResult = "btcusd"
            const result = cryptoUtil.getUri(cryptoType)
            expect(result).to.be.equals(desiredResult)
        })
    
        it("should return ethusd", () => {
            const cryptoType = "eth"
            const desiredResult = "ethusd"
            const result = cryptoUtil.getUri(cryptoType)
            expect(result).to.be.equals(desiredResult)
        })
    })

    describe("Test calculateAmount", () => {

        it("should return 100", () => {
            const cryptoPrice = 50
            const amount = 2
            const desiredResult = cryptoPrice * amount
            const result = cryptoUtil.calculateAmount(cryptoPrice, amount)
            expect(result).to.be.equals(desiredResult)
        })

        it("should return 20", () => {
            const cryptoPrice = 100
            const amount = 0.2
            const desiredResult = cryptoPrice * amount
            const result = cryptoUtil.calculateAmount(cryptoPrice, amount)
            expect(result).to.be.equals(desiredResult)
        })

        it("should return 0.8", () => {
            const cryptoPrice = 0.4
            const amount = 2
            const desiredResult = cryptoPrice * amount
            const result = cryptoUtil.calculateAmount(cryptoPrice, amount)
            expect(result).to.be.equals(desiredResult)
        })

        it("should return 0.75", () => {
            const cryptoPrice = 0.25
            const amount = 3
            const desiredResult = cryptoPrice * amount
            const result = cryptoUtil.calculateAmount(cryptoPrice, amount)
            expect(result).to.be.equals(desiredResult)
        })

    })

    describe("Test getPrice", () => {

        //restore spy after it blocks
        afterEach(() => {
            chai.spy.restore()
        })

        it("should get price with BTC without error correctly", async () => {
            //expected return from API
            const expectedReturnedJson = {
                data: {
                    last: 100
                }
            }
            spy.on(apiUtil, ["getApi"], () => {
                return Promise.resolve(expectedReturnedJson)
            })

            const desiredApiUrl = `${env.CRYPTO_PRICE_URL}/btcusd`
            const capitalLetterBTC = "BTC"

            let result = await cryptoUtil.getPrice(capitalLetterBTC)

            expect(apiUtil.getApi).to.have.been.called(1)
            expect(apiUtil.getApi).to.have.been.called.with(desiredApiUrl)
            expect(result.err).to.be.undefined
            expect(result.price).to.be.equals(expectedReturnedJson.data.last)

        })

        it("should get price with btc without error correctly", async () => {
            //expected return from API
            const expectedReturnedJson = {
                data: {
                    last: 100
                }
            }
            spy.on(apiUtil, ["getApi"], () => {
                return Promise.resolve(expectedReturnedJson)
            })

            const desiredApiUrl = `${env.CRYPTO_PRICE_URL}/btcusd`
            const smallLetterBtc = "btc"

            let result = await cryptoUtil.getPrice(smallLetterBtc)

            expect(apiUtil.getApi).to.have.been.called(1)
            expect(apiUtil.getApi).to.have.been.called.with(desiredApiUrl)
            expect(result.err).to.be.undefined
            expect(result.price).to.be.equals(expectedReturnedJson.data.last)

        })


        it("should get price with BTC with error correctly", async () => {
            spy.on(apiUtil, ["getApi"], () => {
                return Promise.reject(new Error("Random Error"))
            })

            const desiredApiUrl = `${env.CRYPTO_PRICE_URL}/btcusd`
            const capitalLetterBTC = "BTC"

            let result = await cryptoUtil.getPrice(capitalLetterBTC)

            expect(apiUtil.getApi).to.have.been.called(1)
            expect(apiUtil.getApi).to.have.been.called.with(desiredApiUrl)
            expect(result.err).to.be.exist
            expect(result.price).to.be.undefined

        })

        it("should get price with ETH without error correctly", async () => {
            //expected return from API
            const expectedReturnedJson = {
                data: {
                    last: 200
                }
            }
            spy.on(apiUtil, ["getApi"], () => {
                return Promise.resolve(expectedReturnedJson)
            })
    
            const desiredApiUrl = `${env.CRYPTO_PRICE_URL}/ethusd`
            const capitalLetterETH = "ETH"
    
            let result = await cryptoUtil.getPrice(capitalLetterETH)
    
            expect(apiUtil.getApi).to.have.been.called(1)
            expect(apiUtil.getApi).to.have.been.called.with(desiredApiUrl)
            expect(result.err).to.be.undefined
            expect(result.price).to.be.equals(expectedReturnedJson.data.last)
    
        })

        it("should get price with eth without error correctly", async () => {
            //expected return from API
            const expectedReturnedJson = {
                data: {
                    last: 200
                }
            }
            spy.on(apiUtil, ["getApi"], () => {
                return Promise.resolve(expectedReturnedJson)
            })
    
            const desiredApiUrl = `${env.CRYPTO_PRICE_URL}/ethusd`
            const smallLetterEth = "eth"
    
            let result = await cryptoUtil.getPrice(smallLetterEth)
    
            expect(apiUtil.getApi).to.have.been.called(1)
            expect(apiUtil.getApi).to.have.been.called.with(desiredApiUrl)
            expect(result.err).to.be.undefined
            expect(result.price).to.be.equals(expectedReturnedJson.data.last)
    
        })

        

        it("should get price with ETH with error correctly", async () => {
        
            spy.on(apiUtil, ["getApi"], () => {
                return Promise.reject(new Error("Random Error"))
            })

            const desiredApiUrl = `${env.CRYPTO_PRICE_URL}/ethusd`
            const capitalLetterETH = "eth"

            let result = await cryptoUtil.getPrice(capitalLetterETH)

            expect(apiUtil.getApi).to.have.been.called(1)
            expect(apiUtil.getApi).to.have.been.called.with(desiredApiUrl)
            expect(result.err).to.be.exist
            expect(result.price).to.be.undefined

        })
    })

})

