const spies = require("chai-spies")
const chai = require("chai")
const cryptoUtil = require("../../src/common/crypto.util")
const userService = require("../../src/user/user.service")
const userRepo = require("../../src/user/user.repo")

chai.use(spies)
const { expect, spy } = require("chai")

describe("Testing user.service.js", () => {
    describe("Test getUserBalance", () => {

        //restore spy after it blocks
        afterEach(() => {
            chai.spy.restore()
        })

        it("should return 0 for empty data", async () => {

            spy.on(userRepo, ["findUserCryptoById"], () => {
                return Promise.resolve([])
            })

            spy.on(cryptoUtil, ["getPrice", "calculateAmount"], () => {
                return undefined
            })

            const userId = "xyz"
            const result = await userService.getUserBalance(userId)

            expect(userRepo.findUserCryptoById).to.have.been.called(1)
            expect(cryptoUtil.getPrice).to.have.been.not.called
            expect(cryptoUtil.calculateAmount).to.have.been.not.called
            expect(userRepo.findUserCryptoById).to.have.been.called.with(userId)
            expect(result).to.be.equals(0)

        })

        it("should return 1000", async () => {

            const btcAmount = 1
            const ethAmount = 5

            const userData = [
                {
                    cryptoType: "btc",
                    amount: btcAmount
                },
                {
                    cryptoType: "eth",
                    amount: ethAmount
                }
            ]

            const btcPrice = 500
            const ethPrice = 100
            
            spy.on(userRepo, ["findUserCryptoById"], () => {
                return Promise.resolve(userData)
            })

            spy.on(cryptoUtil, ["getPrice"], (cryptoType) => {
                let price = undefined;
                switch(cryptoType){
                    case "btc":
                        price = btcPrice
                        break;
                    case "eth": 
                        price = ethPrice
                        break;
                    default:
                        price = 0
                }
                return Promise.resolve({
                    err: undefined,
                    price
                })
            })

            const userId = "abc"
            const desiredResult = (btcAmount * btcPrice) + (ethAmount * ethPrice)
            const result = await userService.getUserBalance(userId)

            expect(userRepo.findUserCryptoById).to.have.been.called(1)
            expect(userRepo.findUserCryptoById).to.have.been.called.with(userId)
            expect(cryptoUtil.getPrice).to.have.been.called(2)
            expect(result).to.be.equals(desiredResult)

        })

        it("should return 1001.8", async () => {

            const btcAmount = 2
            const ethAmount = 2

            const userData = [
                {
                    cryptoType: "btc",
                    amount: btcAmount
                },
                {
                    cryptoType: "eth",
                    amount: ethAmount
                }
            ]

            const btcPrice = 500
            const ethPrice = 0.9
            
            spy.on(userRepo, ["findUserCryptoById"], () => {
                return Promise.resolve(userData)
            })

            spy.on(cryptoUtil, ["getPrice"], (cryptoType) => {
                let price = undefined;
                switch(cryptoType){
                    case "btc":
                        price = btcPrice
                        break;
                    case "eth": 
                        price = ethPrice
                        break;
                    default:
                        price = 0
                }
                return Promise.resolve({
                    err: undefined,
                    price
                })
            })

            const userId = "abc"
            const desiredResult = (btcAmount * btcPrice) + (ethAmount * ethPrice)
            const result = await userService.getUserBalance(userId)

            expect(userRepo.findUserCryptoById).to.have.been.called(1)
            expect(userRepo.findUserCryptoById).to.have.been.called.with(userId)
            expect(cryptoUtil.getPrice).to.have.been.called(2)
            expect(result).to.be.equals(desiredResult)

        })

        it("should throw error for API error", async () => {
            const btcAmount = 2
            const ethAmount = 2

            const userData = [
                {
                    cryptoType: "btc",
                    amount: btcAmount
                },
                {
                    cryptoType: "eth",
                    amount: ethAmount
                }
            ]

            const btcPrice = 500
            const ethPrice = 0.9
            
            spy.on(userRepo, ["findUserCryptoById"], () => {
                return Promise.resolve(userData)
            })

            const apiError = new Error("API Error")
            spy.on(cryptoUtil, ["getPrice"], () => {
                return Promise.reject(apiError)
            })

            const userId = "ghi"

            try{
                await userService.getUserBalance(userId)
            }catch(err){
                expect(err).to.be.exist
                expect(err).to.be.equals(apiError)
            }
            expect(userRepo.findUserCryptoById).to.have.been.called(1)
            expect(userRepo.findUserCryptoById).to.have.been.called.with(userId)
        })

    })
})